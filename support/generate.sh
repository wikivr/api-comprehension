#!/bin/bash 

rm -rf src/gen
mkdir -p src/gen/ && touch src/gen/__init__.py

# generate the protobuf code for the server
python3.6 -m grpc_tools.protoc -I=proto --python_out=src --grpc_python_out=src proto/gen/*.proto
