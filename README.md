# Machine Comprehension Server

Popular Machine Learning and Deep Learning models exist in Python, hence this component is
the python implementation of Machine Comprehension.

## Quick Tips
- Everything is containerized. Start from the `Makefile` to understand the build and running.
- Since this is containerized, please run it on a Linux kernel.
- If you find something non-containerized, raise an issue! It must be containerized.
- There is nothing called local-testing, it must all be tested in containers.

## Contribution
- Clone this repo to anywhere in your machine.
- Checkout into a new branch using `git checkout -b <FEATURE_BRANCH>`
- Develop + Test, once everything looks good, commit.
- Push to your checked out branch in the remote using: `git push origin <FEATURE_BRANCH>`
- Create a Merge Request and make sure you to check `Close Branch after Merge` option.

## Testing
- Go to deployment repo and follow the installation instructions.
- Find the comprehension pod by issuing the following command:
  ```
  $ kubectl -n wikivr get pods
  NAME                             READY   STATUS    RESTARTS   AGE
  comprehension-7d689d579c-jd6ls   1/1     Running   1          6m11s
  scraper-7c5cd864ff-sn6ch         1/1     Running   0          6m11s
  ```
- Login to the comprehension pod with:
  ```
  kubectl -n wikivr exec -it comprehension-7d689d579c-jd6ls -- /bin/bash
  root@comprehension-7d689d579c-bf59h:/home/wikivr/src#
  ```

- Run `python grpc_client.py` to see the following output:
  ```
  root@comprehension-7d689d579c-bf59h:/home/wikivr/src# python grpc_client.py 
  response: "done"
  code: Ok

  answer: "Patient Protection and Affordable Care Act"

  answer: "American attorney and politician"

  answer: "August 4, 1961"
  ```

## Troubleshooting
Will update as discovered
