import argparse
import grpc
import time

from concurrent import futures

from gen import comprehension_pb2
from gen import comprehension_pb2_grpc
from nn.bidaf import bidaf
from utils import fileops
from scraper import wiki


class CommunicationServicer(comprehension_pb2_grpc.CommunicationServicer):
    """
    gRPC server for Communication Service
    """
    def __init__(self, *args, **kwargs):
        self.server_port = kwargs['grpc_port']
        self.bidaf = bidaf.BIDAF()
        self.file_mgr = fileops.FileManager()
        self.scraper = wiki.WikiScraper()

    def Ask(self, request, context):
        """
        Implementation of the rpc Ask declared in the proto file
        """
        # get the handle from the incoming request
        handle = request.handle

        # load the passage from the cached path
        passage = self.file_mgr.read(handle)

        # get the question from the person
        question = request.question

        return comprehension_pb2.QuestionResponse(answer=self.bidaf.predict(question, passage))

    def SetDocument(self, request, context):
        """
        Stores the document for QA purpose
        """
        # get the handle of the document requested
        handle = request.handle

        # get the content of the document requested
        content = request.content

        # scrap the irrelevant information and store only the text
        document, img_url = self.scraper.get_text(content)

        self.file_mgr.save(handle, document)

        return comprehension_pb2.DocumentResponse(response="done", code=comprehension_pb2.Ok)

    def start_server(self):
        """
        Function which actually starts the gRPC server, and preps it for
        serving incoming connections
        """
        # declare a server object with desired number of thread pool workers
        communication_server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

        # this line can be ignored
        comprehension_pb2_grpc.add_CommunicationServicer_to_server(CommunicationServicer(grpc_port=self.server_port),
                communication_server)

        # bind the server to the port defined above
        communication_server.add_insecure_port('[::]:{}'.format(self.server_port))

        # start the server
        communication_server.start()
        print('Comprehension Server running ...')

        try:
            while True:
                time.sleep(60*60*60)
        except KeyboardInterrupt:
            communication_server.stop(0)
            print('Comprehension Server stopped ...')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--comprehension-port", help="port for comprehension server")
    args = parser.parse_args()

    server_port = 6592
    if args.comprehension_port:
        server_port = int(args.comprehension_port)

    curr_server = CommunicationServicer(grpc_port=server_port)
    curr_server.start_server()
