import pathlib
import os

from utils import constants as const


class FileManager(object):

    def __init__(self):
        self._cached_dir = const.CACHED_DIR

        # create cache directory
        pathlib.Path(self._cached_dir).mkdir(exist_ok=True)
    
    def save(self, handle, data):
        file_path = self._get_path(handle)
        with open(file_path, 'w') as f:
            f.write(data)

    def read(self, handle):
        file_path = self._get_path(handle)
        with open(file_path, 'r') as f:
            return f.read()

    def _get_path(self, handle):
        return os.path.join(self._cached_dir, handle)
