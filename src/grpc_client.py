import grpc
import requests

from gen import comprehension_pb2
from gen import comprehension_pb2_grpc

class CommunicationClient(object):
    """
    Client for accessing the gRPC functionality
    """

    def __init__(self):
        self.host = 'localhost'
        self.server_port = 6592

        # instantiate a communication channel
        self.channel = grpc.insecure_channel(
                '{}:{}'.format(self.host, self.server_port))

        # bind the client to the server channel
        self.stub = comprehension_pb2_grpc.CommunicationStub(self.channel)

    def ask_question(self, question):
        request = comprehension_pb2.QuestionRequest(handle=self._handle, question=question)
        return self.stub.Ask(request)

    def set_document(self):
        url = 'https://en.wikipedia.org/w/api.php?action=parse&prop=text&page=%s&format=json' % self._handle
        dummy_text = requests.get(url).content
        request = comprehension_pb2.DocumentRequest(handle=self._handle, content=dummy_text)
        return self.stub.SetDocument(request)

    def set_handle(self, handle):
        self._handle = handle

if __name__ == "__main__":
    client = CommunicationClient()
    client.set_handle("Bhagat_Singh")
    print(client.set_document())
    print(client.ask_question("what are you known for?"))
    print(client.ask_question("who are you?"))
    print(client.ask_question("When were you born?"))
