from allennlp import pretrained

class BIDAF(object):

    def __init__(self):
        self._predictor = pretrained.bidirectional_attention_flow_seo_2017()

    def predict(self, question, passage):
        return self._predictor.predict(question, passage)['best_span_str']
