import json

from bs4 import BeautifulSoup

class WikiScraper(object):
    _HTML_PARSER_TYPE = 'html.parser'

    # Expecting raw Wiki content, such as: https://en.wikipedia.org/w/api.php?action=parse&prop=text&page=Barack%20Obama&format=json
    def get_text(self, content):
        html = self._get_html_content(content)
        soup = BeautifulSoup(html, self._HTML_PARSER_TYPE)
        heading = soup.h2
        paras = heading.find_all_previous("p")
        paraslist = [para.get_text() for para in paras][::-1]

        introtext =  "\n".join(paraslist)
        introimg = soup.img['src']

        return introtext, introimg

    def _get_html_content(self, text):
        try:
            jsonobj = json.loads(text)
            return jsonobj['parse']['text']
        except Exception as e:
            print("failed to parse the wiki output")
            with open('/tmp/text', 'w+') as f:
                f.write(content)
            raise e
