# Registry for building images
REGISTRY ?= wikivr
VERSION ?= latest
COMPREHENSION_IMAGE ?= $(REGISTRY)/comprehension

COMPREHENSION_TAG ?= "$(COMPREHENSION_IMAGE):$(VERSION)"

all: build

build: build_comprehension

build_comprehension:
	docker build --build-arg "VERSION=$(VERSION)" -t $(COMPREHENSION_TAG) -f ./build/Dockerfile .

run:
	docker run --rm --name wikivr_run -v "$$PWD:/home/wikivr/" "$$(docker build -f build/Dockerfile.run --quiet .)"

push:
	docker push $(COMPREHENSION_TAG)
